# User service

User service a simple service for managing users
it allows user to register and login both by email
and using google.

### Run user service locally

`go run cmd/main.go` 

be carefull with all arguments that are required

### Run using docker

`docker run -d --network host stgleb/user-app`

### Run using docker-compose(preffered)

`docker-compose up`

### Run using makefile

`make all`